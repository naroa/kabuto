﻿
namespace kabutoRework
{
    partial class kabuto2
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Local Media");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Network Media");
            this.mnStrp = new System.Windows.Forms.MenuStrip();
            this.mnItmFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItmFiles_AddFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItmFiles_AddFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItmLocal = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItmNetwork = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItmNetwork_Video = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItmNetwork_Audio = new System.Windows.Forms.ToolStripMenuItem();
            this.mnItmMiscellaneous = new System.Windows.Forms.ToolStripMenuItem();
            this.spltCntnrContentViewer = new System.Windows.Forms.SplitContainer();
            this.trVwLibrary = new System.Windows.Forms.TreeView();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.btnRepeat = new System.Windows.Forms.Button();
            this.btnShuffle = new System.Windows.Forms.Button();
            this.btnVolume = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.pnlGui = new System.Windows.Forms.Panel();
            this.mnStrp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltCntnrContentViewer)).BeginInit();
            this.spltCntnrContentViewer.Panel1.SuspendLayout();
            this.spltCntnrContentViewer.SuspendLayout();
            this.pnlControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.pnlGui.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnStrp
            // 
            this.mnStrp.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.mnStrp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnItmFiles,
            this.mnItmLocal,
            this.mnItmNetwork,
            this.mnItmMiscellaneous});
            this.mnStrp.Location = new System.Drawing.Point(0, 0);
            this.mnStrp.Name = "mnStrp";
            this.mnStrp.Size = new System.Drawing.Size(1185, 40);
            this.mnStrp.TabIndex = 0;
            this.mnStrp.Text = "menuStrip";
            // 
            // mnItmFiles
            // 
            this.mnItmFiles.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnItmFiles_AddFile,
            this.mnItmFiles_AddFolder});
            this.mnItmFiles.Name = "mnItmFiles";
            this.mnItmFiles.Size = new System.Drawing.Size(81, 36);
            this.mnItmFiles.Text = "Files";
            // 
            // mnItmFiles_AddFile
            // 
            this.mnItmFiles_AddFile.Name = "mnItmFiles_AddFile";
            this.mnItmFiles_AddFile.Size = new System.Drawing.Size(294, 44);
            this.mnItmFiles_AddFile.Text = "Add a File...";
            // 
            // mnItmFiles_AddFolder
            // 
            this.mnItmFiles_AddFolder.Name = "mnItmFiles_AddFolder";
            this.mnItmFiles_AddFolder.Size = new System.Drawing.Size(294, 44);
            this.mnItmFiles_AddFolder.Text = "Add a folder...";
            // 
            // mnItmLocal
            // 
            this.mnItmLocal.Name = "mnItmLocal";
            this.mnItmLocal.Size = new System.Drawing.Size(187, 36);
            this.mnItmLocal.Text = "Local Playback";
            // 
            // mnItmNetwork
            // 
            this.mnItmNetwork.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnItmNetwork_Video,
            this.mnItmNetwork_Audio});
            this.mnItmNetwork.Name = "mnItmNetwork";
            this.mnItmNetwork.Size = new System.Drawing.Size(223, 36);
            this.mnItmNetwork.Text = "Network Playback";
            // 
            // mnItmNetwork_Video
            // 
            this.mnItmNetwork_Video.Name = "mnItmNetwork_Video";
            this.mnItmNetwork_Video.Size = new System.Drawing.Size(367, 44);
            this.mnItmNetwork_Video.Text = "Open video stream...";
            // 
            // mnItmNetwork_Audio
            // 
            this.mnItmNetwork_Audio.Name = "mnItmNetwork_Audio";
            this.mnItmNetwork_Audio.Size = new System.Drawing.Size(367, 44);
            this.mnItmNetwork_Audio.Text = "Open audio stream...";
            // 
            // mnItmMiscellaneous
            // 
            this.mnItmMiscellaneous.Name = "mnItmMiscellaneous";
            this.mnItmMiscellaneous.Size = new System.Drawing.Size(88, 36);
            this.mnItmMiscellaneous.Text = "Misc.";
            // 
            // spltCntnrContentViewer
            // 
            this.spltCntnrContentViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltCntnrContentViewer.Location = new System.Drawing.Point(0, 0);
            this.spltCntnrContentViewer.Name = "spltCntnrContentViewer";
            // 
            // spltCntnrContentViewer.Panel1
            // 
            this.spltCntnrContentViewer.Panel1.Controls.Add(this.trVwLibrary);
            this.spltCntnrContentViewer.Size = new System.Drawing.Size(1185, 1048);
            this.spltCntnrContentViewer.SplitterDistance = 390;
            this.spltCntnrContentViewer.TabIndex = 1;
            // 
            // trVwLibrary
            // 
            this.trVwLibrary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trVwLibrary.Location = new System.Drawing.Point(0, 0);
            this.trVwLibrary.Name = "trVwLibrary";
            treeNode1.Name = "ndLocal";
            treeNode1.Text = "Local Media";
            treeNode1.ToolTipText = "A list of all media you\'ve browsed locally";
            treeNode2.Name = "ndNetwork";
            treeNode2.Text = "Network Media";
            treeNode2.ToolTipText = "A list of all media you\'ve browsed via network";
            this.trVwLibrary.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.trVwLibrary.Size = new System.Drawing.Size(390, 1048);
            this.trVwLibrary.TabIndex = 0;
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.btnRepeat);
            this.pnlControls.Controls.Add(this.btnShuffle);
            this.pnlControls.Controls.Add(this.btnVolume);
            this.pnlControls.Controls.Add(this.trackBar1);
            this.pnlControls.Controls.Add(this.btnNext);
            this.pnlControls.Controls.Add(this.btnPrevious);
            this.pnlControls.Controls.Add(this.btnStop);
            this.pnlControls.Controls.Add(this.btnPlay);
            this.pnlControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlControls.Location = new System.Drawing.Point(0, 1048);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(1185, 52);
            this.pnlControls.TabIndex = 2;
            // 
            // btnRepeat
            // 
            this.btnRepeat.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRepeat.Location = new System.Drawing.Point(735, 0);
            this.btnRepeat.Name = "btnRepeat";
            this.btnRepeat.Size = new System.Drawing.Size(150, 52);
            this.btnRepeat.TabIndex = 7;
            this.btnRepeat.Text = "Repeat: Off";
            this.btnRepeat.UseVisualStyleBackColor = true;
            // 
            // btnShuffle
            // 
            this.btnShuffle.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnShuffle.Location = new System.Drawing.Point(885, 0);
            this.btnShuffle.Name = "btnShuffle";
            this.btnShuffle.Size = new System.Drawing.Size(150, 52);
            this.btnShuffle.TabIndex = 6;
            this.btnShuffle.Text = "Shuffle: Off";
            this.btnShuffle.UseVisualStyleBackColor = true;
            // 
            // btnVolume
            // 
            this.btnVolume.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnVolume.Location = new System.Drawing.Point(1035, 0);
            this.btnVolume.Name = "btnVolume";
            this.btnVolume.Size = new System.Drawing.Size(150, 52);
            this.btnVolume.TabIndex = 5;
            this.btnVolume.Text = "Volume";
            this.btnVolume.UseVisualStyleBackColor = true;
            // 
            // trackBar1
            // 
            this.trackBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.trackBar1.Location = new System.Drawing.Point(480, 0);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(259, 52);
            this.trackBar1.TabIndex = 4;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnNext.Location = new System.Drawing.Point(360, 0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(120, 52);
            this.btnNext.TabIndex = 3;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPrevious.Location = new System.Drawing.Point(240, 0);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(120, 52);
            this.btnPrevious.TabIndex = 2;
            this.btnPrevious.Text = "Previous";
            this.btnPrevious.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnStop.Location = new System.Drawing.Point(120, 0);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(120, 52);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnPlay
            // 
            this.btnPlay.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPlay.Location = new System.Drawing.Point(0, 0);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(120, 52);
            this.btnPlay.TabIndex = 0;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            // 
            // pnlGui
            // 
            this.pnlGui.Controls.Add(this.spltCntnrContentViewer);
            this.pnlGui.Controls.Add(this.pnlControls);
            this.pnlGui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGui.Location = new System.Drawing.Point(0, 40);
            this.pnlGui.Name = "pnlGui";
            this.pnlGui.Size = new System.Drawing.Size(1185, 1100);
            this.pnlGui.TabIndex = 1;
            // 
            // kabuto2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 1140);
            this.Controls.Add(this.pnlGui);
            this.Controls.Add(this.mnStrp);
            this.MainMenuStrip = this.mnStrp;
            this.MinimumSize = new System.Drawing.Size(1211, 246);
            this.Name = "kabuto2";
            this.Text = "Kabuto";
            this.Load += new System.EventHandler(this.kabuto2_Load);
            this.mnStrp.ResumeLayout(false);
            this.mnStrp.PerformLayout();
            this.spltCntnrContentViewer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltCntnrContentViewer)).EndInit();
            this.spltCntnrContentViewer.ResumeLayout(false);
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.pnlGui.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnStrp;
        private System.Windows.Forms.ToolStripMenuItem mnItmFiles;
        private System.Windows.Forms.ToolStripMenuItem mnItmLocal;
        private System.Windows.Forms.ToolStripMenuItem mnItmNetwork;
        private System.Windows.Forms.ToolStripMenuItem mnItmMiscellaneous;
        private System.Windows.Forms.SplitContainer spltCntnrContentViewer;
        private System.Windows.Forms.TreeView trVwLibrary;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Panel pnlGui;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button btnVolume;
        private System.Windows.Forms.Button btnShuffle;
        private System.Windows.Forms.Button btnRepeat;
        private System.Windows.Forms.ToolStripMenuItem mnItmFiles_AddFile;
        private System.Windows.Forms.ToolStripMenuItem mnItmFiles_AddFolder;
        private System.Windows.Forms.ToolStripMenuItem mnItmNetwork_Video;
        private System.Windows.Forms.ToolStripMenuItem mnItmNetwork_Audio;
    }
}

