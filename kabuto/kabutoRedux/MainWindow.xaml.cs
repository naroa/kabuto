﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace kabutoRedux
{
    /// <summary>
    /// Interaction logic for Gui.xaml
    /// </summary>
    public partial class Gui : Window
    {
        private bool _seekingSliderIsDragging;
        private bool _isShuffling;
        private bool _libraryExists;
        private string _libraryCache;
        private readonly Dictionary<DirectoryInfo, List<FileInfo>> _library = new Dictionary<DirectoryInfo, List<FileInfo>>();

        public Gui()
        {
            InitializeComponent();
            InitializeSlider();
        }

        private void MediaOpened(object sender, RoutedEventArgs e)
        {
            MyMedia.Play();
        }

        private void MediaEnded(object sender, RoutedEventArgs e)
        {

        }

        private void InitializeSlider()
        {
            SeekingSlider.Value = MyMedia.Position.TotalSeconds;
            var timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(200) };
            timer.Tick += TrackMedia;
            timer.Start();
        }

        private void TrackMedia(object sender, EventArgs e)
        {
            if (!_seekingSliderIsDragging)
                SeekingSlider.Value = MyMedia.Position.TotalSeconds;
        }

        private void LibToggle_Click(object sender, RoutedEventArgs e)
        {
            if (_libraryExists)
                LoadLibrary();
            else
                BuildLibrary();
        }

        private void LoadLibrary()
        {
            using (StreamReader cacheReader = new StreamReader(_libraryCache))
            {
                //TODO: implement loading library cache :-)
            }
        }

        private void BuildLibrary()
        {
            //1. alert user what is about to happen
            //2. open a window to select directory user wants to load
            //3. ask user if they want to load all subfolders within this folder

            
        }
    }
}
