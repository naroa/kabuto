﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using System.Windows.Media;
using Microsoft.WindowsAPICodePack.Shell;

namespace kabuto
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
     {
        private List<Uri> _currentPlaylist;
        private int _currentSong;
        private bool _stopAfterCurrent;
        private Uri _baseMusicFolder;
        private readonly TreeViewItem _streams = new TreeViewItem();
        private readonly Dictionary<string,List<Uri>> _library = new Dictionary<string, List<Uri>>();
        private bool _isLibraryPopulated;
        private List<Uri> _currentListView = new List<Uri>();
        private bool _replayAll;
        private bool _replaySingle;
        private string _currentHeader;
        private static readonly List<string> AcceptedExtensions = new List<string> { ".mp3", ".mp4", ".wma", ".wav", ".flac", ".m4a" };
        private static readonly string CacheFolder = Directory.GetCurrentDirectory();
        private readonly FileInfo _libraryCache = new FileInfo(Path.Combine(CacheFolder, "config", "library.kabuto"));
        private readonly FileInfo _settingsCache = new FileInfo(Path.Combine(CacheFolder, "config", "settings.kabuto"));
        private readonly FileInfo _streamsCache = new FileInfo(Path.Combine(CacheFolder, "config", "streams.kabuto"));
        private bool _isShuffling;
        private List<int> _previouslyShuffled;
        private int _currentPositionInShuffle;
        private bool _seekingSliderIsDragging;
        private bool _volumeSliderIsDragging;
        private bool _currentlySearching;
        private object[] _carbonCopy;

        public MainWindow()
        {
            InitializeComponent();
            LoadCachedFiles();
            var cache = new DirectoryInfo(Path.Combine(CacheFolder, "config"));
            if (!cache.Exists)
            {
                cache.Create();
            }
        }

        private enum SongSelector { First, Next, Previous }

        private void LoadCachedFiles()
        {
            if (_libraryCache.Exists)
            {
                using (var reader = _libraryCache.OpenText())
                {
                    var libraryLocation = reader.ReadLine();
                    if (libraryLocation != null)
                        try
                        {
                            _baseMusicFolder = new Uri(libraryLocation);
                        }
                        catch (Exception e)
                        {
                            var errorLoad = new ListViewItem()
                            {
                                Content = $"ERROR LOADING LIBRARY. YOU FUCKED WITH THE CACHE. LOOK AT WHAT YOU DID: {e.Message}",
                                Height = 20,
                                Background = Brushes.Crimson
                            };
                            SongsList.Items.Add(errorLoad);
                            reader.Close();
                            _libraryCache.Delete();
                            return;
                        }
                    reader.Close();
                    if (!_baseMusicFolder.IsAbsoluteUri) return;
                    PopulateLibrary();
                    _isLibraryPopulated = true;
                }
            }
            if (_settingsCache.Exists)
            {
                using (var reader = _settingsCache.OpenText())
                {
                    var cachedVolume = reader.ReadToEnd();
                    cachedVolume = cachedVolume.Substring(cachedVolume.LastIndexOf("Volume:", StringComparison.Ordinal) + 8);
                    try
                    {
                        MyMedia.Volume = double.Parse(cachedVolume);
                        VolumeSlider.Value = MyMedia.Volume*100;
                    }
                    catch (Exception e)
                    {
                        if (VolumeSlider.Value.ToString(CultureInfo.CurrentCulture) !=
                            (MyMedia.Volume*100).ToString(CultureInfo.CurrentCulture))
                        {
                            var errorLoad = new ListViewItem()
                            {
                                Content = $"ERROR LOADING VOLUME. YOU FUCKED WITH THE CACHE. LOOK AT WHAT YOU DID: {e.Message}",
                                Height = 20,
                                Background = Brushes.Crimson
                            };
                            SongsList.Items.Add(errorLoad);
                            reader.Close();
                            MyMedia.Volume = 1;
                            VolumeSlider.Value = 100;
                            _settingsCache.Delete();
                        }
                    }
                }
            }
        }

        private void AddLibraryToCache()
        {
            //if (!_libraryCache.Exists)
            //{
            //    _libraryCache.Create();
            //}
            using(var writer = _libraryCache.CreateText())
            {
                writer.WriteLine(_baseMusicFolder.AbsoluteUri);
                writer.Close();
            }
        }

        private void PopulateLibrary()
        {
            var baseDirectory = new DirectoryInfo(_baseMusicFolder.AbsolutePath);
            SearchSubdirectories(baseDirectory);

            if (!_library.ContainsKey("Streams"))
            {
                _streams.Header = "Streams";
                _library.Add("Streams", new List<Uri>());
                _streams.Selected += (o, args) => FolderClick("Streams", SongSelector.First);
                if (!_streamsCache.Exists)
                {
                    _streamsCache.Create();
                }
                else
                {
                    var streams = new List<string>();
                    using (var reader = _streamsCache.OpenText())
                    {
                        while(reader.EndOfStream==false)
                            streams.Add(reader.ReadLine());
                        reader.Close();
                    }
                    foreach (var stream in streams)
                    {
                        _library["Streams"].Add(new Uri(stream));
                    }
                }
                ArtistsTree.Items.Add(_streams);
            }
            
            _isLibraryPopulated = true;
        }

        private void SearchSubdirectories(DirectoryInfo superDirectory)
        {
            var currentLevel = new TreeViewItem() {Header = superDirectory.ToString()};
            foreach (var subdirectory in superDirectory.GetDirectories())
            {
                var subLevel = new TreeViewItem() {Header = subdirectory.ToString()};
                subLevel.Selected += (o, args) => FolderClick(subdirectory.ToString(), SongSelector.First);
                subLevel.MouseDoubleClick += (o, args) => FolderDoubleClick(subdirectory.ToString());
                currentLevel.Items.Add(subLevel);
                if (subdirectory.GetDirectories().Length != 0)
                {
                    SearchSubdirectories(subdirectory);
                }
                foreach (var song in subdirectory.GetFiles())
                {
                    var extension = song.FullName;
                    extension = extension.Substring(extension.LastIndexOf(".", StringComparison.Ordinal));
                    var playable = IsPlayableFile(extension);
                 
                    var listOfSongs = new List<Uri>();
                    if (_library.ContainsKey(subdirectory.Name))
                    {
                        _library.TryGetValue(subdirectory.Name, out listOfSongs);
                        if (listOfSongs != null)
                        {
                            if (playable)
                                _library[subdirectory.Name].Add(new Uri(song.FullName));
                        }
                    }
                    else
                    {
                        if (!playable)
                        {
                            _library.Add(subdirectory.Name, listOfSongs);
                        }
                        else
                        {
                            listOfSongs.Add(new Uri(song.FullName));
                            _library.Add(subdirectory.Name, listOfSongs);
                        }
                    }
                }
            }
            ArtistsTree.Items.Add(currentLevel);
        }

        private void FolderClick(string header, SongSelector highLightPosition)
        {
            _currentListView = _library[header];
            _currentHeader = header;
            var highlightThis = 0;
            if(SongsList.HasItems)
                SongsList.Items.Clear();

            switch (highLightPosition)
            {
                case SongSelector.First: //TODO: Looking at the behavior here, this enum may be unneeded. A bool would effectively get the same thing done, but we'll hold on to this until we know there isnt anything else we want to do here :)
                {
                    highlightThis = 0;
                    break;
                }
                case SongSelector.Next:
                {
                    highlightThis = _currentSong;
                    break;
                }
                case SongSelector.Previous:
                {
                    highlightThis = _currentSong;
                    break;
                }
                default: //TODO: Should we have something here? I'm not sure. In theory it is impossible to have anything besides First/Next/Previous since I built the enum, but idk.
                {
                    break;
                }
            }

            if (_currentPlaylist == _currentListView)
                highlightThis = _currentSong;

            if (header != "Streams")
            {
                foreach (var song in _library[header])
                {
                    var currentShell = ShellFile.FromFilePath(song.LocalPath);
                    string songName;
                    if (currentShell.Properties.System.Title.Value != null)
                    {
                        songName = currentShell.Properties.System.Title.Value;
                    }
                    else
                    {
                        songName = song.ToString();
                        songName = songName.Substring(songName.LastIndexOf("/", StringComparison.Ordinal) + 1);
                    }
                    var currentSong = highlightThis == 0
                        ? new ListViewItem() {Content = songName, Height = 20, Background = Brushes.Aqua}
                        : new ListViewItem() {Content = songName, Height = 20};
                    highlightThis--;
                    currentSong.MouseDoubleClick += (o, args) => CurrentSong_MouseDoubleClick(song, _library[header]);
                    SongsList.Items.Add(currentSong);
                }
            }
            else
            {
                foreach (var stream in _library[header])
                {
                    var streamName = stream.ToString();
                    var currentSong = highlightThis == 0
                        ? new ListViewItem() {Content = streamName, Height = 20, Background = Brushes.Aqua}
                        : new ListViewItem() {Content = streamName, Height = 20};
                    highlightThis--;
                    currentSong.MouseDoubleClick += (o, args) => CurrentSong_MouseDoubleClick(stream, _library[header]);
                    SongsList.Items.Add(currentSong);
                }
            }
        }

        private void CurrentSong_MouseDoubleClick(Uri songUri, List<Uri> libraryList)
        {
            _currentPlaylist = libraryList;
            _currentSong = libraryList.IndexOf(songUri);
            FolderClick(_currentHeader, SongSelector.Next);
            MyMedia.Source = _currentPlaylist[_currentSong];
            MyMedia.Play();
            _stopAfterCurrent = false;
        }

        private void FolderDoubleClick(string header)
        {
            _currentSong = 0;
            FolderClick(header, SongSelector.First);
            _currentPlaylist = _library[header];
            MyMedia.Source = _currentPlaylist[0];
            MyMedia.Play();
            _stopAfterCurrent = false;
        }

        private void MenuOpenLibrary_Click(object sender, RoutedEventArgs e)
        {
            LibraryEntryBox.Visibility = Visibility.Visible;
            OpenLibraryButton.Visibility = Visibility.Visible;
            CancelOpenLibraryButton.Visibility = Visibility.Visible;
        }

        private void MenuAddFile_Click(object sender, RoutedEventArgs e)
        {
            UnimplementedFunction("adding files");
            //TODO add this function!
        }

        private void MenuAddFolder_Click(object sender, RoutedEventArgs e)
        {
            UnimplementedFunction("adding folders");
            //TODO
        }

        private void MenuImportPlaylist_Click(object sender, RoutedEventArgs e)
        {
            UnimplementedFunction("importing playlists");
            //TODO
        }

        private void MenuExportPlaylist_Click(object sender, RoutedEventArgs e)
        {
            UnimplementedFunction("exporting playlists");
            //TODO
        }

        private void InitializeSlider()
        {
            SeekingSlider.Value = MyMedia.Position.TotalSeconds;
            var timer = new DispatcherTimer {Interval = TimeSpan.FromMilliseconds(200)};
            timer.Tick += TrackMedia;
           /* if (MyMedia.NaturalDuration.HasTimeSpan)
            {
                var ts = MyMedia.NaturalDuration.TimeSpan;
                SeekingSlider.Maximum = ts.TotalSeconds;
                SeekingSlider.SmallChange = 1;
                SeekingSlider.LargeChange = Math.Min(10, ts.Seconds / 10);
            }*/
            timer.Start();
        }

        private void TrackMedia(object sender, EventArgs e)
        {
            if (!_seekingSliderIsDragging)
                SeekingSlider.Value = MyMedia.Position.TotalSeconds;
        }

        private void PlayMedia(object sender, RoutedEventArgs e)
        {
            if (MyMedia.Source.LocalPath.Contains(".mp4"))
            {
                var video = new KabutoVideo(MyMedia.Source);
                video.Show();
                MyMedia.Source = null;
            }
            else
            {
                InitializeSlider();
            }
        }

        private void SeekingSlider_DragStarted(object sender, DragStartedEventArgs e)
        {
            _seekingSliderIsDragging = true;
        }

        private void SeekingSlider_DragEnded(object sender, DragCompletedEventArgs e)
        {
            _seekingSliderIsDragging = false;
            MyMedia.Position = TimeSpan.FromSeconds(SeekingSlider.Value);
        }

        private void VolumeSlider_Update(object sender, RoutedEventArgs e)
        {
            MyMedia.Volume = VolumeSlider.Value/100;
            using (var writer = _settingsCache.CreateText())
            {
                writer.WriteLine($"Volume: {MyMedia.Volume}");
            }
        }

        private void SeekingSlider_Update(object sender, RoutedEventArgs e)
        {
            MyMedia.Position = TimeSpan.FromSeconds(SeekingSlider.Value);
        }

        private void Play_button_Click(object sender, RoutedEventArgs e)
        {
            MyMedia.Play();
            _stopAfterCurrent = false;
        }

        private void Pause_button_Click(object sender, RoutedEventArgs e)
        {
            MyMedia.Pause();
        }

        private void Stop_button_Click(object sender, RoutedEventArgs e)
        {
            MyMedia.Stop();
        }

        private void Previous_button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_isShuffling)
                {
                    if (_currentPositionInShuffle > 1)
                    {
                        _currentPositionInShuffle--;
                        _currentSong = _previouslyShuffled[_currentPositionInShuffle];
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    if (_currentPlaylist[_currentSong - 1] == null)
                    {
                        return;
                    }
                    MyMedia.Source = _currentPlaylist[_currentSong - 1];
                    MyMedia.Play();
                    _currentSong--;
                }
                _stopAfterCurrent = false;
                SelectNextSong(false);
            }
            catch
            {
                if (_replayAll)
                {
                    if (_isShuffling)
                    {
                        if (_currentPositionInShuffle > 1)
                        {
                            _currentPositionInShuffle--;
                            _currentSong = _previouslyShuffled[_currentPositionInShuffle];
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        MyMedia.Source = _currentPlaylist[_currentPlaylist.Count - 1];
                        MyMedia.Play();
                        _currentSong = _currentPlaylist.Count - 1;
                    }
                    _stopAfterCurrent = false;
                    SelectNextSong(false);
                }
                else
                {
                    MyMedia.Stop();
                    MyMedia.Play();
                }
            }
        }

         private void Skip_button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_isShuffling)
                {
                    _previouslyShuffled.Add(_currentSong);
                    while (_previouslyShuffled.Contains(_currentSong) && _previouslyShuffled.Count<_currentPlaylist.Count)
                        _currentSong = new Random(DateTime.Now.Second).Next(_currentPlaylist.Count);
                    _currentPositionInShuffle++;
                    MyMedia.Source = _currentPlaylist[_currentSong];
                    MyMedia.Play();
                }
                else
                {
                    if (_currentPlaylist[_currentSong + 1] == null)
                    {
                        return;
                    }

                    MyMedia.Source = _currentPlaylist[_currentSong + 1];
                    MyMedia.Play();
                    _currentSong++;
                }
                _stopAfterCurrent = false;
                SelectNextSong(true);
            }
            catch
            {
                if (_replayAll)
                {
                    if (_isShuffling)
                    {
                        _previouslyShuffled.Add(_currentSong);
                        _currentSong = new Random(DateTime.Now.Second).Next(_currentPlaylist.Count);
                        _currentPositionInShuffle++;
                        MyMedia.Source = _currentPlaylist[_currentSong];
                        MyMedia.Play();
                    }
                    else
                    {
                        MyMedia.Source = _currentPlaylist[0];
                        MyMedia.Play();
                        _currentSong = 0;
                    }
                    _stopAfterCurrent = false;
                    SelectNextSong(true);
                }
                else
                {
                    MyMedia.Stop();
                }
            }
        }

        private void StopAfterCurrent_Click(object sender, RoutedEventArgs e)
        {
            _stopAfterCurrent = true;
        }

        private void ReplaySingle_Click(object sender, RoutedEventArgs e)
        {
            _replaySingle = _replaySingle != true;
            ReplayButton.Content = ReplayButton.Content.ToString() == "One" ? "Off" : "One";
        }

         private void ReplayAll_Click(object sender, RoutedEventArgs e)
        {
            _replayAll = _replayAll != true;
            ReplayButton.Content = ReplayButton.Content.ToString() == "All" ? "Off" : "All";
        }

        private void Player_MediaEnded(object sender, EventArgs e)
        {
            if (_stopAfterCurrent)
                MyMedia.Stop();
            else
            {
                if (_replaySingle)
                {
                    MyMedia.Source = _currentPlaylist[_currentSong];
                    MyMedia.Position = TimeSpan.Zero;
                    MyMedia.Play();
                    _stopAfterCurrent = false;
                }
                else if (_currentSong+1>_currentPlaylist.Count-1)
                {
                    if (!_replayAll && !_isShuffling) return;
                    if (_isShuffling)
                    {
                        _previouslyShuffled.Add(_currentSong);
                        while (_previouslyShuffled.Contains(_currentSong) && _previouslyShuffled.Count < _currentPlaylist.Count)
                            _currentSong = new Random(DateTime.Now.Second).Next(_currentPlaylist.Count);
                        _currentPositionInShuffle++;
                        MyMedia.Source = _currentPlaylist[_currentSong];
                        MyMedia.Play();
                    }
                    else
                    {
                        MyMedia.Source = _currentPlaylist[0];
                        MyMedia.Play();
                        _currentSong = 0;
                    }
                    _stopAfterCurrent = false;
                    SelectNextSong(true);
                }
                else
                {
                    if (_isShuffling)
                    {
                        _previouslyShuffled.Add(_currentSong);
                        _currentSong = new Random(DateTime.Now.Second).Next(_currentPlaylist.Count);
                        _currentPositionInShuffle++;
                        MyMedia.Source = _currentPlaylist[_currentSong];
                        MyMedia.Play();
                    }
                    else
                    { 
                        MyMedia.Source = _currentPlaylist[_currentSong + 1];
                        MyMedia.Play();
                        _currentSong++;
                    }
                    _stopAfterCurrent = false;
                    SelectNextSong(true);
                }
            }
        }

        private void SelectNextSong(bool forward)
        {
            if (forward)
            {
                if (_library.ContainsValue(_currentPlaylist) && _currentPlaylist == _currentListView)
                {
                   FolderClick(_currentHeader, SongSelector.Next);
                }
            }
            else
            {
                if (_library.ContainsValue(_currentPlaylist) && _currentPlaylist == _currentListView)
                {
                    FolderClick(_currentHeader, SongSelector.Previous);
                }
            }
        }

       /* private void volumeup_Click(object sender, RoutedEventArgs e)
        {
            if (MyMedia.Volume <= .95)
                MyMedia.Volume = MyMedia.Volume + .05;
            else if (Math.Abs(MyMedia.Volume) < 1)
            {
                MyMedia.Volume = 1;
            }
            else
            {
                return;
            }
            //if (!_settingsCache.Exists)
            //    _settingsCache.Create();
            using (var writer = _settingsCache.CreateText())
            {
                writer.WriteLine($"Volume: {MyMedia.Volume}");
            }
        }
        //These two functions are now outdated since we've taken out the +/- volume buttons. :)
        private void volumedown_Click(object sender, RoutedEventArgs e)
        {
            if (MyMedia.Volume >= .05)
                MyMedia.Volume = MyMedia.Volume - .05;
            else if (Math.Abs(MyMedia.Volume) > 0)
            {
                MyMedia.Volume = 0;
            }
            else
            {
                return;
            }
            //if (!_settingsCache.Exists)
            //    _settingsCache.Create();
            using (var writer = _settingsCache.CreateText())
            {
                writer.WriteLine($"Volume: {MyMedia.Volume}");
            }
        }*/

        private void OpenAudioStream_Click(object sender, RoutedEventArgs e)
        {
            AudioStreamEntryBox.Visibility = Visibility.Visible;
            StartAudioStreamButton.Visibility = Visibility.Visible;
            CancelAudioStreamButton.Visibility = Visibility.Visible;
        }

        private void OpenVideoStream_Click(object sender, RoutedEventArgs e)
        {
            UnimplementedFunction("streaming video");
             //TODO
        }

        private void StartAudioStreamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var audioStream = new Uri(AudioStreamEntryBox.Text);
                if (_library.ContainsKey("Streams"))
                {
                    if (!_library["Streams"].Contains(audioStream))
                    {
                        _library["Streams"].Add(audioStream);
                        using (var writer = _streamsCache.AppendText())
                        {
                            writer.WriteLine(audioStream.AbsoluteUri);
                        }
                    }
                }
                else //TODO: this else statement SHOULD be superfluous right now. Going to keep it in just in case it, for some reason, is needed.
                {
                    _library.Add("Streams",new List<Uri>());
                    _library["Streams"].Add(audioStream);
                    _streams.Header = "Streams";
                    _streams.Selected += (o, args) => FolderClick("Streams", SongSelector.First);
                    ArtistsTree.Items.Add(_streams);
                }
                MyMedia.Source = audioStream;
                MyMedia.Play();
                AudioStreamEntryBox.Visibility = Visibility.Collapsed;
                StartAudioStreamButton.Visibility = Visibility.Collapsed;
                CancelAudioStreamButton.Visibility = Visibility.Collapsed;
            }
            catch
            {
                AudioStreamEntryBox.Text = "Invalid URL";
            }
        }

        private void CancelAudioStreamButton_Click(object sender, RoutedEventArgs e)
        {
            AudioStreamEntryBox.Visibility = Visibility.Collapsed;
            StartAudioStreamButton.Visibility = Visibility.Collapsed;
            CancelAudioStreamButton.Visibility = Visibility.Collapsed;
        }

        private void OpenLibraryButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _baseMusicFolder = new Uri(LibraryEntryBox.Text);
                LibraryEntryBox.Visibility = Visibility.Collapsed;
                OpenLibraryButton.Visibility = Visibility.Collapsed;
                CancelOpenLibraryButton.Visibility = Visibility.Collapsed;
                if (_baseMusicFolder.IsAbsoluteUri && _isLibraryPopulated == false)
                {
                    PopulateLibrary();
                    AddLibraryToCache();
                }
            }
            catch
            {
                LibraryEntryBox.Text = "Invalid URL";
            }
        }

        private void CancelOpenLibraryButton_Click(object sender, RoutedEventArgs e)
        {
            LibraryEntryBox.Visibility = Visibility.Collapsed;
            OpenLibraryButton.Visibility = Visibility.Collapsed;
            CancelOpenLibraryButton.Visibility = Visibility.Collapsed;
        }

        private void MenuOpenFile_Click(object sender, RoutedEventArgs e)
        {
            PlayLocalFile.Visibility = Visibility.Visible;
            CancelLocalFile.Visibility = Visibility.Visible;
            LocalMediaEntryBox.Visibility = Visibility.Visible;
            AddLocalToPlayList.Visibility = Visibility.Visible;
        }

        private void PlayLocalFile_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var mediaStream = new Uri(LocalMediaEntryBox.Text);
                if (mediaStream.IsAbsoluteUri)
                {
                    MyMedia.Source = mediaStream;
                    MyMedia.Play();
                    _stopAfterCurrent = false;
                    _currentPlaylist = new List<Uri>();
                    AddToPlayList(MyMedia.Source);
                }
                else
                    throw new ArgumentException();
            }
            catch
            {
                LocalMediaEntryBox.Text = "Invalid path";
            }
        }

        private void AddLocalFile_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var mediaStream = new Uri(LocalMediaEntryBox.Text);
                if (mediaStream.IsAbsoluteUri)
                {
                    AddToPlayList(mediaStream);
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            catch
            {
                LocalMediaEntryBox.Text = "Invalid path";
            }
        }

        private void CancelLocalFile_OnClick(object sender, RoutedEventArgs e)
        {
            PlayLocalFile.Visibility = Visibility.Collapsed;
            CancelLocalFile.Visibility = Visibility.Collapsed;
            LocalMediaEntryBox.Visibility = Visibility.Collapsed;
            AddLocalToPlayList.Visibility = Visibility.Collapsed;
        }

        private void SearchFunction(object sender, RoutedEventArgs e)
        {
            SearchEntryBox.Visibility = Visibility.Visible;
            ExecuteSearch.Visibility = Visibility.Visible;
            CancelSearch.Visibility = Visibility.Visible;
        }

        private void StartQuery_OnClick(object sender, RoutedEventArgs e)
        {
            //I'm thinking the most logical way to implement this in a fairly clean manner will be to overlay a new library view over the default one. This means there will also have to be another button
            //added to the UI so that the user has a clean way of backing out of the search view back into "normal" view. This makes the most sense to me, I think.
            //The biggest issue I'm predicting is that then the cleanest way to play music from that view would be to duplicate the player functions just for this new view, which is gross and dumb.
            //Maybe what I can do is copy over the ArtistsTree to a temp tree so we don't have to reconstruct data once we go into search mode, then recreate the ArtistsTree for searching. Once search is closed
            //then we can pull back from the temp tree to get a faster recovery? I think this makes the most sense to do, so I'll run with it and see what happens. :)

            _currentlySearching = true;

            var count = ArtistsTree.Items.Count;
            _carbonCopy = new object[count];

            ArtistsTree.Items.CopyTo(_carbonCopy, 0);
            //this copies over the old tree to hold on it so we can easily go back to it after the search is done :)

            ArtistsTree.Items.Clear();
            //Blows away the artists tree since we're gonna be building a new, different one! With hookers and blackjack!

            var query = SearchEntryBox.Text;

            foreach (var item in _carbonCopy) //This for method is for checking artist matches.
            {
                if (_library.ContainsKey(query)) //Adds an artist DIRECT match to the success.
                {
                    var itemCopy = item;

                    ArtistsTree.Items.Add(itemCopy);
                }
                else if (item.ToString().Contains(query)) //Adds an artist SOFT match to the success.
                {
                    var itemCopy = item;
                    
                    ArtistsTree.Items.Add(itemCopy);
                }
            }

            if (SongsList.HasItems)
                SongsList.Items.Clear();
            foreach (var songList in _library.Values)
            {
                _currentListView = new List<Uri>();
                foreach (var song in songList)
                {
                    if (song.ToString().Contains(query))
                    {
                        _currentListView.Add(song);
                        SongsList.Items.Add(new ListViewItem() {Content = song, Height = 20});
                    }
                }
            }

            //Current issues/bugs:
            //Can't play individual songs found through search. (initial SongList that shows up)
            //Can't search by album. <-- Not sure how to fix this one...
            //Artist search doesn't reset in between searches, so you can currently only pare down results from already found results rather than having a clean new search
        }

        private void CancelQuery_OnClick(object sender, RoutedEventArgs e)
        {
            SearchEntryBox.Visibility = Visibility.Collapsed;
            ExecuteSearch.Visibility = Visibility.Collapsed;
            CancelSearch.Visibility = Visibility.Collapsed;
        }

        private void ResetSearch_OnClick(object sender, RoutedEventArgs e)
        {
            if (_currentlySearching)
            {
                ArtistsTree.Items.Clear();
                foreach (var item in _carbonCopy)
                    ArtistsTree.Items.Add(item);
            }
        }

        private void AddToPlayList(Uri song)
        {
            if (_currentPlaylist == null)
            {
                _currentPlaylist = new List<Uri>();
                _currentSong = 0;
            }
            _currentPlaylist.Add(song);
        }

        private void LibToggle_OnClick(object o, RoutedEventArgs e)
        {
            ArtistsTree.Visibility = ArtistsTree.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            SongsList.Visibility = SongsList.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        private static bool IsPlayableFile(string fileExtension)
        {
            return AcceptedExtensions.Contains(fileExtension.ToLower());
        }

        private void ReplayButton_OnClick(object sender, RoutedEventArgs e)
        {
            switch (ReplayButton.Content.ToString())
            {
            case "Off":
                {
                    ReplayButton.Content = "One";
                    _replaySingle = true;
                    break;
                }
            case "One":
                {
                    ReplayButton.Content = "All";
                    _replaySingle = false;
                    _replayAll = true;
                    break;
                }
            case "All":
                {
                    ReplayButton.Content = "Off";
                    _replayAll = false;
                    break;
                }
            default:
                {
                    break;
                }
            }
        }

        private void Shuffle_Click(object sender, RoutedEventArgs e)
        {
            _isShuffling = !_isShuffling;

            if (_isShuffling)
            {
                _previouslyShuffled= new List<int>();
                _currentPositionInShuffle = 0;
            }
        }

        private void UnimplementedFunction(string function)
        {
            var notImplemented = new ListViewItem
            {
                Content = $"Thanks for the interest in {function}! Unfortunately, we haven't implemented this yet :(        ",
                Height = 20,
                Background = Brushes.Magenta
            };
            SongsList.Items.Add(notImplemented);
        }

        private void OpenFAQ_Click(object sender, RoutedEventArgs e)
        {
            List<string> questionList = new List<string>
            {
                "Why are there these .dll files packaged with the zip?        ",
                "Why can't I add files or folders individually?         ",
                "Why is the window locked to this small window?         ",
                "I found a bug! What can I do to help?!         ",
                "Where's \"x\" feature I have in \"y\" media player, I'd love to see it here!",
                "Why can't I right-click on anything?",
                "Why can I only pause/play and fullscreen video?",
                "WAIT YOU CAN FULLSCREEN THE VIDEO PLAYER????",
                "This looks ugly.",
                "So, why did you make this?",
                "Who's working on this?",
                "What's in store for the future?",
                "My question isn't here?"
            };
            List<string> answerList = new List<string>
            {
                "They're required files we had to pass along with you since we don't have an automatic installer. Don't worry, they're safe, but don't get rid of them or Kabuto won't work!         ",
                "This is a planned feature for the very near future! We wanted feedback on what we already have out, so we figured we'd ship our first test build out without it. Sorry for any inconvenience!         ",
                "Since this is just our first test build, there are a lot of bugs we already know we have to fix. This was a temporary solution to one of the first ones we found. Once we can make the UI stretch correctly, you'll have window freedom again!         ",
                "Awesome! This was the goal of this test release! We prefer to field all bug-reports from our dev email so we know where to find what we need to fix: hgsdevs@gmail.com. Also found in the \"Help\" tab!         ",
                "So would we! Once again, we prefer to get all of our feedback related to improving the product at our email: hgsdevs@gmail.com         ",
                "Well, you can... It just doesn't do anything yet! Context menus(that thing that pops up when you usually right click something) are coming soon :)         ",
                "As you may have noticed, the video player is in a separate window. This window is going to become a LOT more in the future, and we promise you'll love it. Hang in there~!         ",
                "Yes! Double left click on the video and the player will go fullscreen. Double left click again and it'll shrink back down. Enjoy!         ",
                "Hey, questions only! But, yes, it is very ugly :( We don't have a graphic designer. This is our first official development build, please have mercy! (And email us if you want to help us out on this ;) )         ",
                "We really, really don't like iTunes and Windows Media Player. Every other media player doesn't do enough, and those do too much with a GUI we really don't like. So we're doing something about it by making our own! Our dreams won't be memes.         ",
                "Two college comp sci students! We work on this on our free time as a pair of nerds who love music. This is our first official project together, and likely not the last!         ",
                "Lots! We have tons of ideas left to implement, but we were so excited to get this out to you all that we just wanted to get something bare minimum out. Kabuto is going to become so much more in the future, so stay tuned, patient, and awesome!         ",
                "We lied a little bit about this being an FAQ... These questions were just thought up ahead of this release. But we'd like to make this an official FAQ! Send us questions at hgsdevs@gmail.com and we'll get back to you ASAP. Thank you so much for your curiosity!         "
            };
            for (var i = 0; i < questionList.Count; i++)
            {
                var question = new ListViewItem
                {
                    Content = questionList[i],
                    Height = 20,
                    //Background = Brushes.Magenta
                };
                SongsList.Items.Add(question);
                var answer = new ListViewItem
                {
                    Content = answerList[i],
                    Height=20,
                    Background = Brushes.LawnGreen    
                };
                SongsList.Items.Add(answer);
            }
        }

        private void VolToggle_OnClick(object o, RoutedEventArgs e)
        {
            VolumeSlider.Visibility = VolumeSlider.Visibility == Visibility.Hidden ? Visibility.Visible : Visibility.Hidden;
        }
     }
}
