﻿using System;
using System.Windows;
using System.Windows.Input;


namespace kabuto
{
    /// <summary>
    /// Interaction logic for KabutoVideo.xaml
    /// </summary>
    public partial class KabutoVideo : Window
    {
        private bool _isPlaying;

        public KabutoVideo()
        {
            InitializeComponent();
        }

        public KabutoVideo(Uri videoUri)
        {
            InitializeComponent();
            VideoPlayer.Source = videoUri;
            VideoPlayer.Play();
            _isPlaying = true;
        }

        private void TogglePlay(object sender, RoutedEventArgs e)
        {
            if (_isPlaying)
            {
                _isPlaying = false;
                VideoPlayer.Pause();
            }
            else
            {
                _isPlaying = true;
                VideoPlayer.Play();
            }
        }

        private void ToggleFullscreen(object sender, MouseButtonEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowStyle = WindowStyle.SingleBorderWindow;
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowStyle = WindowStyle.None;
                WindowState = WindowState.Maximized;
            }
        }
    }
}
